"" Install vim-plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"" Enable sythax highlighting
syntax enable

"" Ruler and numbers
set ruler
set number

"" 256 Colors
set t_Co=256

"" Remove whitespace after saving
autocmd BufWritePre * :%s/\s\+$//e
autocmd BufWritePre * :%s/\t/  /e

"" Favorite color scheme
color molokai
"" color github

"" whitespace
set expandtab
set tabstop=2 shiftwidth=2
set nowrap
set backspace=indent,eol,start

"" That red marker for 80 characters
set colorcolumn=131

set nocompatible
set encoding=utf-8
set showcmd
filetype plugin indent on

"" Powerline
set laststatus=2

"" Search configuration
set hlsearch
set incsearch
set ignorecase
set smartcase

"" Start pathogen
call pathogen#infect()

"" Command-P
set wildignore+=coverage*
set wildignore+=*/tmp/*
set wildignore+=*/node_modules/*
set wildignore+=public/*
let g:CommandTMaxHeight=15

let g:ctrlp_custom_ignore = 'node_modules'

"" Store all .swp files on /tmp
set dir=/tmp

"" NERDTree
map <leader>n :NERDTreeToggle<CR>
"" Open if no file is provided
"" autocmd vimenter * if !argc() | NERDTree | endif
"" Close it if its the last window open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
autocmd VimEnter * wincmd p

"" Load user configuration
if filereadable(expand("~/.vimrc.local"))
  source ~/.vimrc.local
endif

" A few CTRL commands
map <C-S> :w<cr>
map <C-L> =>
""map <C-t> :CtrlP<cr>

" Numbers
nnoremap <F3> :NumbersToggle<CR>

nmap <D-r> :!rails test %<cr>

let g:go_fmt_autosave = 0

" ARROW KEYS ARE UNACCEPTABLE
map <Left> :echo "Use h instead of the Left Arrow!"<cr>
map <Right> :echo "Use l instead of the Right Arrow!"<cr>
map <Up> :echo "Use k instead of the Up Arrow!"<cr>
map <Down> :echo "Use j instead of the Down Arrow!"<cr>

call plug#begin('~/.vim/bundle')

Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'junegunn/vim-easy-align'
Plug 'elixir-editors/vim-elixir'
Plug 'ervandew/supertab'
Plug 'isruslan/vim-es6'
Plug 'fatih/vim-go'
Plug 'powerline/powerline'
Plug 'vim-ruby/vim-ruby'
Plug 'myusuf3/numbers.vim'
Plug 'danro/rename.vim'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tpope/vim-bundler'
Plug 'mattn/emmet-vim'

call plug#end()

map <C-t> :Files<cr>

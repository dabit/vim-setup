# My vim-setup

Only tested with MacVim setup, no idea if it works on something else

## Install

Clone it into ~/.vim folder

    git clone https://gitlab.com/dabit/vim-setup.git .vim

Then create symlinks for the config files

    cd
    ln -s ~/.vim/gvimrc .gvimrc
    ln -s ~/.vim/vimrc .vimrc

Finally, open vim and run:

    :PlugInstall

## Your own settings

Add them to _~/.vimrc.local_

Done

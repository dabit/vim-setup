"" Remove those ugly buttons at the top
set guioptions=aAce

"" Set Command-T to actually use Command-T key binding
macmenu &File.New\ Tab key=<D-T>
map <D-t> :Files<CR>
imap <D-t> <Esc>:Files<CR>

"" Favorite font/size
""set gfn=Bitstream\ Vera\ Sans\ Mono:h20
set gfn=Hack:h18

" Command-/ to toggle comments
map <D-/> <plug>NERDCommenterToggle<CR>
imap <D-/> <Esc><plug>NERDCommenterToggle<CR>i

set macligatures
set guifont=Fira\ Code:h18
set guifont=Hack:h18
"" set guifont=Operator\ Mono:h16
